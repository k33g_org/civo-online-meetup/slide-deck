---
marp: true
---
# One cloud IDE to rule them all
*in less than 15 minutes* ⏱

![w:128 h:128](/pictures/gitpod.png)
![w:128 h:128](/pictures/civo.png)
![w:128 h:128](/pictures/gitlab.png)

*5 more slides, that's all*

---
