<!--
_backgroundColor: whitesmoke
_color: grey
paginate: true
footer: '${FOOTER}'
-->
# Steps - Init

- Import the **cluster management template** https://gitlab.com/k33g_org/project.templates/civo-cluster-management.git
  > - This project has the 2 files `.gitpod.yml` and `.gitpod.dockerfile`
- Open the project with **Gitpod** and wait a moment ⏳



<!-- speaker notes -->
---
