<!--
_backgroundColor: whitesmoke
_color: grey
paginate: true
footer: '${FOOTER}'
-->
# Back to Gitpod

- Update `./applications/gitlab-runner/values.yaml` with the **registration token**
- Update the content of `./helmfile.yaml` to install the needed applications 
- 🚀 commit to tigger the CI, and then deploy the **Kubernetes Executor** on the **Civo** cluster
  > - 👀 the **GitLab** pipeline
  > - 👀 **K9S**
- 👀 CI/CD Settings of the Group
  > - uncheck shared runners
  > - allow the runner to pick jobs without tags
<!-- speaker notes -->
---
