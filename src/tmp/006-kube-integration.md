<!--
_backgroundColor: whitesmoke
_color: grey
paginate: true
footer: '${FOOTER}'
-->
# GitLab/Kubernetes Integration

## Kubernetes integration

**GitLab** works with or within **Kubernetes** in three distinct ways. These can all be used independently or together:
> https://about.gitlab.com/solutions/kubernetes/

- Deploy software from GitLab CI/CD pipelines to Kubernetes
- Use Kubernetes to manage runners attached to your GitLab instance
  > - deploy runners on K8S
- Run the GitLab application and services on a Kubernetes cluster


<!-- speaker notes -->
---
