<!--
_backgroundColor: whitesmoke
_color: grey
paginate: true
footer: '${FOOTER}'
-->
# Create cluster (⏱ 3 minutes)

- Update `.env`
- Run `01-create-cluster.sh`
  > - `civo kubernetes create`
  > - `civo --region=${CLUSTER_REGION} kubernetes config ${CLUSTER_NAME} > ./config/k3s.yaml`
- Run `02-integration-preamble.sh`
  > - Get the PEM certificate
  > - Apply the service account and cluster role binding to the cluster
  > - Get the access token
- 🎉 `k9s --all-namespaces`
<!-- speaker notes -->
---
