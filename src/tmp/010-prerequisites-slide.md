<!--
_backgroundColor: whitesmoke
_color: grey
paginate: true
footer: '${FOOTER}'
-->
# Prerequisites

- Create a **GitLab** group https://gitlab.com/k33g_org/civo-online-meetup
- Set environment variables on **Gitpod** https://gitpod.io/variables
  - `CIVO_API_KEY` from **Civo** https://www.civo.com/account/profile


<!-- speaker notes -->
---
