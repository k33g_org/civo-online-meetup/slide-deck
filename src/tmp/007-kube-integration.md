<!--
_backgroundColor: whitesmoke
_color: grey
paginate: true
footer: '${FOOTER}'
-->
# GitLab/Kubernetes Integration

## Cluster Management Template
> https://docs.gitlab.com/ee/user/clusters/management_project.html

- A project can be used as the management project for a cluster
- This project is created from a  **Cluster Management Template** (provided by GitLab)
- This management project can be used to deploy application to the cluster (ex: runners)

<!-- speaker notes -->
---
