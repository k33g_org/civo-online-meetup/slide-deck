<!--
_backgroundColor: whitesmoke
_color: grey
paginate: true
footer: '${FOOTER}'
-->
# Back to GitLab

- Integrate Kubernetes with a cluster certificate
  > - With parent group
- Add a **Base Domain**
  > - `212.2.247.168.nip.io` / *"Free wildcard DNS services"*
- Go to **Advanced Settings**
  > - set the **cluster management project**
- Copy the CI/CD registration token
<!-- speaker notes -->
---
