<!--
_backgroundColor: #08811E
_color: whitesmoke
paginate: true
footer: '${FOOTER}'
-->
# 🚀 Demo time

From **GitPod**
- 🚧 Create a **Civo** cluster
- 🔧 Setup the **GitLab** Kubernetes Integration
  - 🖐 with a "Cluster Management Project" **designed to work smoothly with Civo** 🥰
- 🚢 Deploy a WebApplication on **Civo** (with **GitLab CI**)

<!-- speaker notes -->
---
