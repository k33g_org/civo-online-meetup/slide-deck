---
marp: true
---
# One cloud IDE to rule them all
*in less than 15 minutes* ⏱

![w:128 h:128](/pictures/gitpod.png)
![w:128 h:128](/pictures/civo.png)
![w:128 h:128](/pictures/gitlab.png)

*5 more slides, that's all*

---
<!--
_backgroundColor: whitesmoke
_color: #08194A
paginate: true
footer: '☸️ Civo online meetup'
-->
# 👋 Hello 😃 & Thank you 😍

## Philippe Charrière | 🐦 @k33g_org | 🦊 @k33g

## TAM @ 🦊 GitLab (with 🇫🇷 accent)

## TAM?

<!-- speaker notes -->
---
<!--
_backgroundColor: whitesmoke
_color: #08194A
paginate: true
footer: '☸️ Civo online meetup'
-->
# Fun Facts

## My 🇬🇧 English is "special"

## I'm a Gitpod addict 😍

## My K8S skills are terrible (worst than my 🇬🇧)
### *This is my main problem*

###### *btw, I'm emojis addict too*


<!-- speaker notes -->
---
<!--
_backgroundColor: #17B7DB
_color: whitesmoke
paginate: true
footer: '☸️ Civo online meetup'
-->
# Civo saved me 🎉

<!-- speaker notes -->
---
<!--
_backgroundColor: #EF9824
_color: whitesmoke
paginate: true
footer: '☸️ Civo online meetup'
-->
# I do everything with Gitpod 💪

<!-- speaker notes -->
---
<!--
_backgroundColor: #08811E
_color: whitesmoke
paginate: true
footer: '☸️ Civo online meetup'
-->
# 🚀 Demo time

From **GitPod**
- 🚧 Create a **Civo** cluster
- 🔧 Setup the **GitLab** Kubernetes Integration
  - 🖐 with a "Cluster Management Project" **designed to work smoothly with Civo** 🥰
- 🚢 Deploy a WebApplication on **Civo** (with **GitLab CI**)

<!-- speaker notes -->
---
